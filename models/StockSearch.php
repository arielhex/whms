<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Stock;

/**
 * StockSearch represents the model behind the search form of `app\models\Stock`.
 */
class StockSearch extends Stock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['product_id', 'warehouse_id', 'zone_id', 'level_id', 'rack_id', 'product_attribute_value_id', 'created_at', 'updated_at'], 'safe'],
            [['quantity_in_hand'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stock::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'quantity_in_hand' => $this->quantity_in_hand,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'product_id', $this->product_id])
            ->andFilterWhere(['like', 'warehouse_id', $this->warehouse_id])
            ->andFilterWhere(['like', 'zone_id', $this->zone_id])
            ->andFilterWhere(['like', 'level_id', $this->level_id])
            ->andFilterWhere(['like', 'rack_id', $this->rack_id])
            ->andFilterWhere(['like', 'product_attribute_value_id', $this->product_attribute_value_id]);

        return $dataProvider;
    }
}
