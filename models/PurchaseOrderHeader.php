<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_order_header".
 *
 * @property int $id
 * @property string $suplier_id
 * @property string $purchase_date
 * @property string $total_amount
 * @property string $created_at
 * @property string $updated_at
 */
class PurchaseOrderHeader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_order_header';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'suplier_id'], 'required'],
            [['id'], 'integer'],
            [['purchase_date', 'created_at', 'updated_at'], 'safe'],
            [['total_amount'], 'number'],
            [['suplier_id'], 'string', 'max' => 40],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'suplier_id' => 'Suplier ID',
            'purchase_date' => 'Purchase Date',
            'total_amount' => 'Total Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
