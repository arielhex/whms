<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_attribute_value".
 *
 * @property int $id
 * @property string $product_id
 * @property string $attribute_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductAttributeValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attribute_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'attribute_id'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_id', 'attribute_id'], 'string', 'max' => 40],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'attribute_id' => 'Attribute ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
