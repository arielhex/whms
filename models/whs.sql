DROP TABLE IF EXISTS sites;
CREATE TABLE sites (
    id INT NOT NULL AUTO_INCREMENT,
    site_code varchar(40) NOT NULL,
    site_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS warehouse;
CREATE TABLE warehouse (
    id INT NOT NULL AUTO_INCREMENT,
    site_id INT NOT NULL,
    warehouse_code varchar(40) NOT NULL,
    warehouse_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS item_category;
CREATE TABLE item_category (
    id INT NOT NULL AUTO_INCREMENT,
    category_code varchar(40) NOT NULL,
    category_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS item_group;
CREATE TABLE item_group (
    id INT NOT NULL AUTO_INCREMENT,
    group_code varchar(40) NOT NULL,
    group_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS generic_product;
CREATE TABLE generic_product (
    id INT NOT NULL AUTO_INCREMENT,
    group_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product;
CREATE TABLE product (
    id INT NOT NULL AUTO_INCREMENT,
    product_code varchar(40) NOT NULL,
    category_id varchar(40) NOT NULL,
    group_id varchar(40) NOT NULL,
    brand_id varchar(40) NOT NULL,
    generic_id varchar(40) NOT NULL,
    model_id varchar(40) NOT NULL,
    product_name varchar(255) NOT NULL,
    product_description varchar(255) DEFAULT NULL,
    product_price decimal DEFAULT NULL,
    has_instances boolean DEFAULT FALSE,
    has_lots boolean DEFAULT FALSE,
    has_attributes varchar(50) DEFAULT NULL,
    default_uom varchar(50) DEFAULT NULL,
    pack_size varchar(255) DEFAULT NULL,
    average_cost decimal DEFAULT NULL,
    single_unit_product_code varchar(40) DEFAULT NULL,
    dimension_group varchar(40) DEFAULT NULL,
    lot_information varchar(40) DEFAULT NULL,
    warranty_terms varchar(255) DEFAULT NULL,
    is_active boolean DEFAULT TRUE,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    delete_at DATETIME,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_attribute_type;
CREATE TABLE product_attribute_type (
    id INT NOT NULL AUTO_INCREMENT,
    attribute_name varchar(255) DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_attribute;
CREATE TABLE product_attribute (
    id INT NOT NULL AUTO_INCREMENT,
    product_id varchar(40) NOT NULL,
    attribute_id varchar(40) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_attribute_value;
CREATE TABLE product_attribute_value (
    id INT NOT NULL AUTO_INCREMENT,
    product_id varchar(40) NOT NULL,
    attribute_id varchar(40) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_instance;
CREATE TABLE product_instance (
    id INT NOT NULL AUTO_INCREMENT,
    product_id varchar(40) NOT NULL,
    serial_number varchar(255) NOT NULL,
    brand_id varchar(40) NOT NULL,
    stock_id varchar(40) NOT NULL,
    lot_information varchar(40) DEFAULT NULL,
    warranty_terms varchar(255) DEFAULT NULL,
    product_attribute_value_id varchar(40) DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_lot;
CREATE TABLE product_lot (
    id INT NOT NULL AUTO_INCREMENT,
    lot_code varchar(40) NOT NULL,
    date_manufatured DATETIME,
    date_expiry DATETIME,
    product_attribute_value_id varchar(40) DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS brand;
CREATE TABLE brand (
    id INT NOT NULL AUTO_INCREMENT,
    manufacture_id varchar(40) NOT NULL,
    brand_code varchar(40) NOT NULL,
    brand_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS brand_manufacturer;
CREATE TABLE brand_manufacturer (
    id INT NOT NULL AUTO_INCREMENT,
    manufacturer_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS stock;
CREATE TABLE stock (
    id INT NOT NULL AUTO_INCREMENT,
    product_id varchar(40) NOT NULL,
    warehouse_id varchar(40) NOT NULL,
    zone_id varchar(40) DEFAULT NULL,
    level_id varchar(40) DEFAULT NULL,
    rack_id varchar(40) DEFAULT NULL,
    quantity_in_hand decimal NULL DEFAULT NULL,
    product_attribute_value_id varchar(40) DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_price_records;
CREATE TABLE product_price_records (
    id INT NOT NULL AUTO_INCREMENT,
    product_id varchar(40) NOT NULL,
    from_date DATETIME,
    product_price DECIMAL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS purchase_order_header;
CREATE TABLE purchase_order_header (
    id INT NOT NULL AUTO_INCREMENT,
    suplier_id varchar(40) NOT NULL,
    purchase_date DATETIME,
    total_amount DECIMAL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS purchase_order_line;
CREATE TABLE purchase_order_line (
    id INT NOT NULL AUTO_INCREMENT,
    po_id varchar(40) NOT NULL,
    product_id INT NOT NULL,
    unit_price DECIMAL DEFAULT NULL,
    quantity DECIMAL DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS suplier;
CREATE TABLE supplier (
    id INT NOT NULL AUTO_INCREMENT,
    supplier_code varchar(40) NOT NULL,
    supplier_name varchar(255) NOT NULL,
    supplier_type varchar(40) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_uom;
CREATE TABLE product_uom (
    id INT NOT NULL AUTO_INCREMENT,
    uom_name varchar(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS product_uom_conversion;
CREATE TABLE product_uom_conversion (
    id INT NOT NULL AUTO_INCREMENT,
    from_uom_id varchar(40) NOT NULL,
    to_uom_id varchar(40) NOT NULL,
    conversion_rule varchar(100) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    username varchar(45) NOT NULL,
    password varchar(45) NOT NULL,
    auth_key varchar(32) NOT NULL,
    password_reset_token varchar(255) DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
