<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "warehouse".
 *
 * @property int $id
 * @property int $site_id
 * @property string $warehouse_code
 * @property string $warehouse_name
 * @property string $created_at
 * @property string $updated_at
 */
class Warehouse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'warehouse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_id', 'warehouse_code', 'warehouse_name'], 'required'],
            [['id', 'site_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['warehouse_code'], 'string', 'max' => 40],
            [['warehouse_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'warehouse_code' => 'Warehouse Code',
            'warehouse_name' => 'Warehouse Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
