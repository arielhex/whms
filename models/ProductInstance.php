<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_instance".
 *
 * @property int $id
 * @property string $product_id
 * @property string $serial_number
 * @property string $brand_id
 * @property string $stock_id
 * @property string $lot_information
 * @property string $warranty_terms
 * @property string $product_attribute_value_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductInstance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_instance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'serial_number', 'brand_id', 'stock_id'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_id', 'brand_id', 'stock_id', 'lot_information', 'product_attribute_value_id'], 'string', 'max' => 40],
            [['serial_number', 'warranty_terms'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'serial_number' => 'Serial Number',
            'brand_id' => 'Brand ID',
            'stock_id' => 'Stock ID',
            'lot_information' => 'Lot Information',
            'warranty_terms' => 'Warranty Terms',
            'product_attribute_value_id' => 'Product Attribute Value ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
