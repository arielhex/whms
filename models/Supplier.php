<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "supplier".
 *
 * @property int $id
 * @property string $supplier_code
 * @property string $supplier_name
 * @property string $supplier_type
 * @property string $created_at
 * @property string $updated_at
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'supplier_code', 'supplier_name', 'supplier_type'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['supplier_code', 'supplier_type'], 'string', 'max' => 40],
            [['supplier_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_code' => 'Supplier Code',
            'supplier_name' => 'Supplier Name',
            'supplier_type' => 'Supplier Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
