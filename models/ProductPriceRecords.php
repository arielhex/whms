<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_price_records".
 *
 * @property int $id
 * @property string $product_id
 * @property string $from_date
 * @property string $product_price
 * @property string $created_at
 * @property string $updated_at
 */
class ProductPriceRecords extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_price_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id'], 'required'],
            [['id'], 'integer'],
            [['from_date', 'created_at', 'updated_at'], 'safe'],
            [['product_price'], 'number'],
            [['product_id'], 'string', 'max' => 40],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'from_date' => 'From Date',
            'product_price' => 'Product Price',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
