<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_uom_conversion".
 *
 * @property int $id
 * @property string $from_uom_id
 * @property string $to_uom_id
 * @property string $conversion_rule
 * @property string $created_at
 * @property string $updated_at
 */
class ProductUomConversion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_uom_conversion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from_uom_id', 'to_uom_id', 'conversion_rule'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['from_uom_id', 'to_uom_id'], 'string', 'max' => 40],
            [['conversion_rule'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_uom_id' => 'From Uom ID',
            'to_uom_id' => 'To Uom ID',
            'conversion_rule' => 'Conversion Rule',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
