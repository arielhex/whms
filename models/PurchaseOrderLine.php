<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_order_line".
 *
 * @property int $id
 * @property string $po_id
 * @property int $product_id
 * @property string $unit_price
 * @property string $quantity
 * @property string $created_at
 * @property string $updated_at
 */
class PurchaseOrderLine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_order_line';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'po_id', 'product_id'], 'required'],
            [['id', 'product_id'], 'integer'],
            [['unit_price', 'quantity'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['po_id'], 'string', 'max' => 40],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'po_id' => 'Po ID',
            'product_id' => 'Product ID',
            'unit_price' => 'Unit Price',
            'quantity' => 'Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
