<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "generic_product".
 *
 * @property int $id
 * @property string $group_name
 * @property string $created_at
 * @property string $updated_at
 */
class GenericProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'generic_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_name'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['group_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
