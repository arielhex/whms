<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $product_code
 * @property string $category_id
 * @property string $group_id
 * @property string $brand_id
 * @property string $generic_id
 * @property string $model_id
 * @property string $product_name
 * @property string $product_description
 * @property string $product_price
 * @property int $has_instances
 * @property int $has_lots
 * @property string $has_attributes
 * @property string $default_uom
 * @property string $pack_size
 * @property string $average_cost
 * @property string $single_unit_product_code
 * @property string $dimension_group
 * @property string $lot_information
 * @property string $warranty_terms
 * @property int $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $delete_at
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_code', 'category_id', 'group_id', 'brand_id', 'generic_id', 'model_id', 'product_name'], 'required'],
            [['id'], 'integer'],
            [['product_price', 'average_cost'], 'number'],
            [['created_at', 'updated_at', 'delete_at'], 'safe'],
            [['product_code', 'category_id', 'group_id', 'brand_id', 'generic_id', 'model_id', 'single_unit_product_code', 'dimension_group', 'lot_information'], 'string', 'max' => 40],
            [['product_name', 'product_description', 'pack_size', 'warranty_terms'], 'string', 'max' => 255],
            [['has_instances', 'has_lots', 'is_active'], 'string', 'max' => 1],
            [['has_attributes', 'default_uom'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_code' => 'Product Code',
            'category_id' => 'Category ID',
            'group_id' => 'Group ID',
            'brand_id' => 'Brand ID',
            'generic_id' => 'Generic ID',
            'model_id' => 'Model ID',
            'product_name' => 'Product Name',
            'product_description' => 'Product Description',
            'product_price' => 'Product Price',
            'has_instances' => 'Has Instances',
            'has_lots' => 'Has Lots',
            'has_attributes' => 'Has Attributes',
            'default_uom' => 'Default Uom',
            'pack_size' => 'Pack Size',
            'average_cost' => 'Average Cost',
            'single_unit_product_code' => 'Single Unit Product Code',
            'dimension_group' => 'Dimension Group',
            'lot_information' => 'Lot Information',
            'warranty_terms' => 'Warranty Terms',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'delete_at' => 'Delete At',
        ];
    }
}
