<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_group".
 *
 * @property int $id
 * @property string $group_code
 * @property string $group_name
 * @property string $created_at
 * @property string $updated_at
 */
class ItemGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_code', 'group_name'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['group_code'], 'string', 'max' => 40],
            [['group_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_code' => 'Group Code',
            'group_name' => 'Group Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
