<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brand".
 *
 * @property int $id
 * @property string $manufacture_id
 * @property string $brand_code
 * @property string $brand_name
 * @property string $created_at
 * @property string $updated_at
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacture_id', 'brand_code', 'brand_name'], 'required'],
            [['manufacture_id', 'brand_code'], 'string', 'max' => 40],
            [['brand_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manufacture_id' => 'Manufacture ID',
            'brand_code' => 'Brand Code',
            'brand_name' => 'Brand Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
