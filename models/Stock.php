<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stock".
 *
 * @property int $id
 * @property string $product_id
 * @property string $warehouse_id
 * @property string $zone_id
 * @property string $level_id
 * @property string $rack_id
 * @property string $quantity_in_hand
 * @property string $product_attribute_value_id
 * @property string $created_at
 * @property string $updated_at
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'warehouse_id'], 'required'],
            [['id'], 'integer'],
            [['quantity_in_hand'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_id', 'warehouse_id', 'zone_id', 'level_id', 'rack_id', 'product_attribute_value_id'], 'string', 'max' => 40],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'warehouse_id' => 'Warehouse ID',
            'zone_id' => 'Zone ID',
            'level_id' => 'Level ID',
            'rack_id' => 'Rack ID',
            'quantity_in_hand' => 'Quantity In Hand',
            'product_attribute_value_id' => 'Product Attribute Value ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
