<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sites".
 *
 * @property int $id
 * @property string $site_code
 * @property string $site_name
 * @property string $created_at
 * @property string $updated_at
 */
class Sites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_code', 'site_name'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['site_code'], 'string', 'max' => 40],
            [['site_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_code' => 'Site Code',
            'site_name' => 'Site Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
