<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brand_manufacturer".
 *
 * @property int $id
 * @property string $manufacturer_name
 * @property string $created_at
 * @property string $updated_at
 */
class BrandManufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand_manufacturer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_name'], 'required'],
                      
            [['manufacturer_name'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacturer_name' => 'Manufacturer Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
