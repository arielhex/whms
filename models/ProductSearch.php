<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['product_code', 'category_id', 'group_id', 'brand_id', 'generic_id', 'model_id', 'product_name', 'product_description', 'has_instances', 'has_lots', 'has_attributes', 'default_uom', 'pack_size', 'single_unit_product_code', 'dimension_group', 'lot_information', 'warranty_terms', 'is_active', 'created_at', 'updated_at', 'delete_at'], 'safe'],
            [['product_price', 'average_cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_price' => $this->product_price,
            'average_cost' => $this->average_cost,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'delete_at' => $this->delete_at,
        ]);

        $query->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'category_id', $this->category_id])
            ->andFilterWhere(['like', 'group_id', $this->group_id])
            ->andFilterWhere(['like', 'brand_id', $this->brand_id])
            ->andFilterWhere(['like', 'generic_id', $this->generic_id])
            ->andFilterWhere(['like', 'model_id', $this->model_id])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_description', $this->product_description])
            ->andFilterWhere(['like', 'has_instances', $this->has_instances])
            ->andFilterWhere(['like', 'has_lots', $this->has_lots])
            ->andFilterWhere(['like', 'has_attributes', $this->has_attributes])
            ->andFilterWhere(['like', 'default_uom', $this->default_uom])
            ->andFilterWhere(['like', 'pack_size', $this->pack_size])
            ->andFilterWhere(['like', 'single_unit_product_code', $this->single_unit_product_code])
            ->andFilterWhere(['like', 'dimension_group', $this->dimension_group])
            ->andFilterWhere(['like', 'lot_information', $this->lot_information])
            ->andFilterWhere(['like', 'warranty_terms', $this->warranty_terms])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        return $dataProvider;
    }
}
