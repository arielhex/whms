<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_lot".
 *
 * @property int $id
 * @property string $lot_code
 * @property string $date_manufatured
 * @property string $date_expiry
 * @property string $product_attribute_value_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductLot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_lot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lot_code'], 'required'],
            [['id'], 'integer'],
            [['date_manufatured', 'date_expiry', 'created_at', 'updated_at'], 'safe'],
            [['lot_code', 'product_attribute_value_id'], 'string', 'max' => 40],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lot_code' => 'Lot Code',
            'date_manufatured' => 'Date Manufatured',
            'date_expiry' => 'Date Expiry',
            'product_attribute_value_id' => 'Product Attribute Value ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
