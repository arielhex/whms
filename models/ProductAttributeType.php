<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_attribute_type".
 *
 * @property int $id
 * @property string $attribute_name
 * @property string $created_at
 * @property string $updated_at
 */
class ProductAttributeType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attribute_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['attribute_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute_name' => 'Attribute Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
