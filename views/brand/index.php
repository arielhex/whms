<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BrandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brands';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-index">
    <div class="header-list">
        <div class="row">
            <div class="col-md-8 col-lg-7">
                <h2><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="col-md-4 col-lg-5 text-right">
                <div class="btn-action text-right">
                    <?= Html::a('<i class="fas fa-pencil-alt"></i>New Brand', ['create'], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Action', ['create'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            'manufacture_id',
            'brand_code',
            'brand_name',
        ],
        'rowOptions'=>function ($model, $key, $index, $grid) {
            $class = $index % 2 ? 'odd' : 'even';
            return array('key' => $key, 'index' => $index, 'class' => $class);
        }
    ]); ?>
</div>
