<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="sidebar">
        <div class="sidebar-brand">
            <i class="fas fa-arrow-left"></i>
        </div>
        <?php
                echo Nav::widget([
                    'options' => ['class' => 'sidebar-nav'],
                    'encodeLabels' => false,
                    'items' => [
                        ['label' => '<i class="fab fa-elementor"></i><span>Manufacture</span>', 'url' => ['/brand-manufacturer'], 'active' => $this->context->route == 'brand-manufacturer/index'],
                        ['label' => '<i class="fas fa-box"></i><span>Brand</span>', 'url' => ['/brand'], 'active' => $this->context->route == 'brand/index'],
                        ['label' => '<i class="fas fa-box-open"></i><span>Sites</span>', 'url' => ['/sites'], 'active' => $this->context->route == 'sites/index'],
                        ['label' => '<i class="fas fa-building"></i><span>Warehouse</span>', 'url' => ['/warehouse'], 'active' => $this->context->route == 'warehouse/index'],
                        ['label' => '<i class="fas fa-clipboard-list"></i><span>Suplier</span>', 'url' => ['/suplier'], 'active' => $this->context->route == 'suplier/index'],
                        ['label' => '<i class="fas fa-file-alt"></i><span>Generic Product</span>', 'url' => ['/generic-product'], 'active' => $this->context->route == 'generic-product/index'],
                        ['label' => '<i class="fas fa-folder-open"></i><span>Item Category</span>', 'url' => ['/item-category'], 'active' => $this->context->route == 'item-category/index'],
                        ['label' => '<i class="fas fa-briefcase"></i><span>Item Group</span>', 'url' => ['/item-group'], 'active' => $this->context->route == 'item-group/index'],
                        ['label' => '<i class="fas fa-clipboard-check"></i><span>Product</span>', 'url' => ['/product'], 'active' => $this->context->route == 'product/index'],
                        ['label' => '<i class="fas fa-cart-arrow-down"></i><span>Purchase</span>', 'url' => ['/purchase'], 'active' => $this->context->route == 'purchase/index'],
                        ['label' => '<i class="fas fa-dolly"></i><span>Stock</span>', 'url' => ['/stock'], 'active' => $this->context->route == 'stock/index'],
                        Yii::$app->user->isGuest ? (
                        ['label' => 'Login', 'url' => ['/site/login']]
                        ) : (
                            '<li>'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')',
                                ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>'
                        )
                    ],
                ]);
        ?>
    </div>

    <div class="header">
        <div class="search-box">
            <i class="fas fa-search"></i>
        </div>
        <div class="logo">
            LOGO
        </div>
        <div class="user-menu">
            <a href="#" class="btn-plus">+</a>
            <a href="#" class="gavatar"><img src="https://secure.gravatar.com/avatar/b34cc7f2742518bba271e0f8f60de056"></a>
        </div>
    </div>
    <div class="main-container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
