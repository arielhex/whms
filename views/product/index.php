<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'product_code',
            'category_id',
            'group_id',
            'brand_id',
            //'generic_id',
            //'model_id',
            //'product_name',
            //'product_description',
            //'product_price',
            //'has_instances',
            //'has_lots',
            //'has_attributes',
            //'default_uom',
            //'pack_size',
            //'average_cost',
            //'single_unit_product_code',
            //'dimension_group',
            //'lot_information',
            //'warranty_terms',
            //'is_active',
            //'created_at',
            //'updated_at',
            //'delete_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
