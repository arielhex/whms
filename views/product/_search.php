<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'product_code') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'group_id') ?>

    <?= $form->field($model, 'brand_id') ?>

    <?php // echo $form->field($model, 'generic_id') ?>

    <?php // echo $form->field($model, 'model_id') ?>

    <?php // echo $form->field($model, 'product_name') ?>

    <?php // echo $form->field($model, 'product_description') ?>

    <?php // echo $form->field($model, 'product_price') ?>

    <?php // echo $form->field($model, 'has_instances') ?>

    <?php // echo $form->field($model, 'has_lots') ?>

    <?php // echo $form->field($model, 'has_attributes') ?>

    <?php // echo $form->field($model, 'default_uom') ?>

    <?php // echo $form->field($model, 'pack_size') ?>

    <?php // echo $form->field($model, 'average_cost') ?>

    <?php // echo $form->field($model, 'single_unit_product_code') ?>

    <?php // echo $form->field($model, 'dimension_group') ?>

    <?php // echo $form->field($model, 'lot_information') ?>

    <?php // echo $form->field($model, 'warranty_terms') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'delete_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
