<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'product_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'generic_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'has_instances')->textInput() ?>

    <?= $form->field($model, 'has_lots')->textInput() ?>

    <?= $form->field($model, 'has_attributes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'default_uom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pack_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'average_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'single_unit_product_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dimension_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lot_information')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warranty_terms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'delete_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
