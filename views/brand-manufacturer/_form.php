<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BrandManufacturer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-manufacturer-form">

    <?php $form = ActiveForm::begin(); ?>

    

    <?= $form->field($model, 'manufacturer_name')->textInput(['maxlength' => true]) ?>

    
    <div class="form-group btn-save">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
