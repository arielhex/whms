<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\BrandManufacturer */

$this->title = 'Create Brand Manufacturer';
$this->params['breadcrumbs'][] = ['label' => 'Brand Manufacturers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-5">
        <div class="brand-manufacturer-create form-create">

            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'model' => $model,
        ]) ?>

        </div>
    </div>
    <div class="col-lg-7">
        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => false,
                'columns' => [
                    'manufacturer_name',
                ],
        'rowOptions'=>function ($model, $key, $index, $grid) {
            $class = $index % 2 ? 'odd' : 'even';
            return array('key' => $key, 'index' => $index, 'class' => $class);
        }
    ]); ?>
    </div>
</div>
