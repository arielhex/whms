<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BrandManufacturer */

$this->title = 'Update Brand Manufacturer: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Brand Manufacturers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="brand-manufacturer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
